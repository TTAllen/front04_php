<?php
class Person {
    var $name;
    function __construct($n) {
        $this->name = $n;
        echo $this->name . '建立<br/>';
    }
    function __destruct() {
        echo $this->name . '解構<br/>';
    }
}
$p = new Person('p');
unset($p);
$q = new Person('q');

